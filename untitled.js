const express = require("express");
const app = express();
const port = 4000;

app.get('/',(req,res)=>{
	res.send("Hello World from our first ExpressJS API!")
})


//mock data
let users = [

	{
		username: "tinaRCBC",
		email:"tinaLRCBC@gmail.com",
		password: "tinaRCBC990"
	},
	{
		username: "gwenstacy1",
		email: "spidergwen@gmail.com",
		password: "gwenOGspider"
	}


];


app.get('/hello',(req,res)=>{

	res.send('Hello from Batch 152');

})

app.get('/users',(req,res)=>{

	//res.send() already stringifies for you.
	res.send(users);

})

app.post('/users',(req,res)=>{

	//With the use of express.json(), we simply have to access the body property of our request object to get the body/input of the request.
	//The receiving of data and the parsing of JSON to JS object has already been done by express.json()

	//Note: When dealing with a route that receives data from a request body is a good practice to check the incoming data from the client.
	console.log(req.body);

	//simulate creating a new user document
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	//push newUser into the users array
	users.push(newUser);
	console.log(users);

	//send the updated the users array in the client
	res.send(users);

})


//delete user route
app.delete('/users',(req,res)=>{

	users.pop();
	console.log(users);

	res.send(users)

})

//update user route
app.put('/users',(req,res)=>{

	//Updating our user will require us to add an input from our client
	console.log(req.body);

	//parseInt the value of the number coming from req.params
	let index = parseInt(req.params.index);

	// //get the index from your request body first
	// users[req.body.index].password = req.body.password

	// //send the updated user to client
	// res.send(users[req.body.index])

})

// GET method requests should not have a request body. It may have 
// headers for additional info. We can pass amount of data somewhere else:
// Through the url
// Route params are values we can pass via the URL.
// This is done specifically to allow us to send small amount of data into
// our server through the request URL.
app.get('/users/getSingleUser/:index',(req,res)=>{
	console.log(req.params);
})

console.log(req.params);

let index = parseInt(req.params.index);
console.log(index);

res.send(users[index])

// app.listen(port,()=>console.log(`Server is running at port ${port}`));


let items = [

	{
		name: "Stick-O",
		price: 70,
		isActive: true
	},	
	{
		name: "Doritos",
		price: 150,
		isActive: true
	}

];

app.get('/items',(req,res)=>{

	res.send(items);

})

app.post('/items',(req,res)=>{
	console.log(req.body);
	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	items.push(newItem);
	console.log(items);
	res.send(items);
})

app.put('/items',(req,res)=>{

	console.log(req.body);
	// items[0].price
	items[req.body.index].price = req.body.price

	res.send(items[req.body.index])

})


app.listen(port,()=>console.log(`Server is running at port ${port}`));




